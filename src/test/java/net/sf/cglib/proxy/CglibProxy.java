/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package net.sf.cglib.proxy;

import java.lang.reflect.Method;

/**
 * <p>cglib代理类的demo</p>
 *
 * @author langxing
 * @date 2021/7/9 22:02
 */
public class CglibProxy implements MethodInterceptor {

    /** cglib 中的 Enhancer */
    private Enhancer enhancer = new Enhancer();

    public Object getProxy(Class clazz){
        // 指定生成的代理父类
        enhancer.setSuperclass(clazz);
        // 设置Callback对象
        enhancer.setCallback(this);
        // 通过字节码技术动态创建子类实例
        return enhancer.create();
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        System.out.println("前置处理");
        // 调用父类中的方法
        Object result = proxy.invokeSuper(obj,args);
        System.out.println("后置处理");
        return result;
    }
}
