/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package net.sf.cglib.proxy;

/**
 * <p>cglib动态代理目标类</p>
 *
 * @author langxing
 * @date 2021/7/9 22:10
 */
public class CGLibTest {
    /**
     * 目标方法
     */
    public String method(String str){
       System.out.println("str:"+str);
       return "CGLibTest.method:"+str;
    }

    public static void main(String[] args) {
        CglibProxy proxy = new CglibProxy();
        // 生成 CGLibTest 的代理对象
        CGLibTest proxyImp = (CGLibTest)proxy.getProxy(CGLibTest.class);
        // 调用代理对象的 method() 方法
        String result = proxyImp.method("test");
        System.out.println(result);
        // 输出结果如下
        // --------------------------
        // 前置处理
        // str:test
        // 后置处理
        // CGLibTest.method:test
    }
}
