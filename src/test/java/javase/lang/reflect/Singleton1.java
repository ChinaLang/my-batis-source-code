/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package javase.lang.reflect;

/**
 * <p>饿汉式-单例模式</p>
 *
 * @author langxing
 * @date 2021/5/9 22:03
 */
public class Singleton1 {

    // 在类加载的时候就创建了单例类对象
    private final static Singleton1 instance = new Singleton1();

    private Singleton1(){}

    public static Singleton1 newInstance(){
        return instance;
    }
}
