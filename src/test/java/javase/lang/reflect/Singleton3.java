/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package javase.lang.reflect;

/**
 * <p>懒汉式-单例模式</p>
 * 静态内部类进行创建
 * @author langxing
 * @date 2021/5/9 22:16
 */
public class Singleton3 {
    // 私有的静态内部类，该静态内部类只会在newInstance方法中被使用
    private static class SingletonHolder{
        // 静态字段
        private static Singleton3 instance = new Singleton3();
    }

    private Singleton3(){}

    public static Singleton3 newInstance(){
        return SingletonHolder.instance;
    }
}
