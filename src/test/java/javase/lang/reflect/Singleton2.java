/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package javase.lang.reflect;

/**
 * <p>懒汉式-单例模式</p>
 * 双重检测方法
 * @author langxing
 * @date 2021/5/9 22:08
 */
public class Singleton2 {
    // 使用volatile关键字修饰instance
    private static volatile Singleton2 instance;

    private Singleton2(){}

    public static Singleton2 newInstance(){
        // 双重检测
        if (instance == null){
            synchronized(Singleton2.class){
                if(instance==null){
                    instance = new Singleton2();
                }
            }
        }
        return instance;
    }
}
