/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package javase.lang.reflect;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * <p>JDK动态代理</p>
 *
 * @author langxing
 * @date 2021/2/23 21:51
 */
public class InvocationHandlerTest {

    @Test
    public void showInvokerHandler(){
        System.out.println("开始代理案例");
        Subject subject = new RealSubject();
        TestInvokerHandler invokerHandler = new TestInvokerHandler(subject);
        // 获取代理对象
        Object objProxy = invokerHandler.getProxy();
        Assertions.assertTrue(objProxy instanceof Subject);
        Subject proxy = (Subject) objProxy;
        // 调用代理对象的方法，它会调用 TestInvokerHandler.invoke()方法
        proxy.operation();
    }

    public class TestInvokerHandler implements InvocationHandler {

        /**
         * 真正的业务对象
         */
        private Object target;

        public TestInvokerHandler(Object target) {
            this.target = target;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("准备处理");
            // ...在执行业务方法之前的预处理...
            Object result = method.invoke(target, args);
            // ...在执行业务方法之后的后置处理...
            System.out.println("处理完成");
            return result;
        }

        public Object getProxy(){
            // 创建代理对象
            // 第一个参数：加载动态生成的代理类的类加载器
            // 第二个参数：业务类实现的接口
            // 第三个参数：InvocationHandler的对象
            return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                    target.getClass().getInterfaces(), this);
        }
    }

    public interface Subject{
        void operation();
    }
    public class RealSubject implements Subject{

        @Override
        public void operation() {
            System.out.println("这里是RealSubject的operation()方法");
        }
    }
}

