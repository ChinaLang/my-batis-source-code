/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package javassist.util.proxy;

import javassist.*;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.net.URLDecoder;

/**
 * <p>javassist的dome</p>
 * 执行如下代码后会生成 javassist.util.dao.JavassistDao.class文件
 * @author langxing
 * @date 2021/7/10 9:26
 */
public class JavassistDemoTest {

    /**
     * 创建 “javassist.util.dao.JavassistDemo”
     */
    @Test
    public void testCreatClass() throws Exception{
        // 创建ClassPool
        ClassPool cp = ClassPool.getDefault();
        // 要生成的类名称为 javassist.util.dao.JavassistDao
        CtClass clazz = cp.makeClass("javassist.util.dao.JavassistDemo");

        StringBuffer body = null;
        // 创建字段，指定了字段类型，字段名称，字段所属的类
        CtField field = new CtField(cp.get("java.lang.String"), "prop", clazz);
        // 指定该字段使用 private 修饰
        field.setModifiers(Modifier.PRIVATE);

        // 设置 prop 字段的 getter/setter 方法
        clazz.addMethod(CtNewMethod.setter("setProp", field));
        clazz.addMethod(CtNewMethod.getter("getProp", field));
        // 设置 prop 字段的初始化值，并将 prop 字段添加到 clazz 中
        clazz.addField(field, CtField.Initializer.constant("MyName"));

        // 创建构造方法，指定了构造方法的参数类型和构造方法所属的类
        CtConstructor ctConstructor = new CtConstructor(new CtClass[]{}, clazz);
        // 设置方法体
        body = new StringBuffer();
        body.append("{\n prop=\"MyName\";\n}");
        ctConstructor.setBody(body.toString());
        // 将构造方法添加到 clazz 中
        clazz.addConstructor(ctConstructor);

        body = new StringBuffer();
        // 创建 execute()方法，指定了方法返回值，方法名称，方法参数列表以及方法所属的类
        CtMethod ctMethod = new CtMethod(CtClass.voidType, "execute", new CtClass[]{}, clazz);
        // 指定该方法使用public修饰
        body.append("{\n System.out.println(\"execute:\"+prop);");
        body.append("\n}");
        ctMethod.setBody(body.toString());
        // 将 execute() 方法添加到 clazz 中
        clazz.addMethod(ctMethod);
        // 将上面定义的 JavassistDao 类保存到指定的目录
        String path = this.getClass().getClassLoader().getResource("").getPath();
        path = URLDecoder.decode(path,"UTF-8");
        System.out.println(path);
        clazz.writeFile(path);

        // 加载 clazz 类，并创建对象
        Class<?> c = clazz.toClass();
        Object o = c.newInstance();
        // 调用 execute() 方法
        Method method = o.getClass().getMethod("execute", new Class[]{});
        method.invoke(o, new Object[]{});

    }


    /**
     * 代理 javassist.util.dao.JavassistDemo
     * @throws Exception
     */
    @Test
    public void testJavassistProxy() throws Exception {
        ProxyFactory factory = new ProxyFactory();
        // 指定父类，ProxyFactory 会动态生成继承该父类的子类方法
        factory.setSuperclass(compile("javassist.util.dao.JavassistDemo"));
        // 设置过滤器，判断那些方法调用需要被拦截
        factory.setFilter(new MethodFilter(){
            @Override
            public boolean isHandled(Method m) {
                if(m.getName().equals("execute")) return true;
                return false;
            }
        });

        // 设置拦截处理
        factory.setHandler(new MethodHandler() {
            @Override
            public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
                System.out.println("前置处理");
                Object result = proceed.invoke(self, args);
                System.out.println("执行后结果:"+result);
                System.out.println("后置处理");
                return result;
            }
        });

        // 创建 JavassistDao 的代理类，并创建代理对象
        Class<?> c = factory.createClass();
        Object demo = c.newInstance();
        // 执行 execute() 方法，会被拦截
        Method method = c.getMethod("execute");
        method.invoke(demo);
        // demo.execute();
        method = c.getMethod("getProp");
        System.out.println(method.invoke(demo));
    }

    public Class<?> compile(String name) {
        ClassLoader loader = this.getClass().getClassLoader();

        try {
            return loader.loadClass(name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return Object.class;
    }

}
