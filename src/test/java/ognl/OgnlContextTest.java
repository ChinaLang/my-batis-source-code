/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package ognl;

import ognl.domain.Author;
import ognl.domain.Blog;
import ognl.domain.Post;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OgnlContextTest {

    private static Blog blog;
    private static Author author;
    private static List<Post> posts;
    private static OgnlContext context;

    @BeforeAll
    public static void start(){
        Blog.staticField = "static Filed";
        author = new Author(1, "userName1", "pwd1", "linhx@qq.com");
        Post post = new Post();
        post.setAuthor(author);
        post.setContent("postContent");
        posts = new ArrayList<>();
        posts.add(post);
        blog = new Blog(1, "主题1", author, posts);

        context =new OgnlContext(null, null, new OgnlMemberAccess());
        context.put("blog", blog);
        context.setRoot(blog);
    }

    @Test
    public void test1() throws OgnlException {
        Author author2 = new Author(2, "userName2", "pwd2", "hx@136.com");
        context.put("author", author2);

        // Ognl.paraseExpression()方法负责解析 OGNL 表达式, author 是 root 对象(即 blog 对象）的属性
        Object obj = Ognl.getValue(Ognl.parseExpression("author"), context, context.getRoot());

        // 输出 Author{id=1, username='userName1', password='pwd1', email='linhx@qq.com'}
        System.out.println(obj);

        obj = Ognl.getValue(Ognl.parseExpression("author.username"), context, context.getRoot());

        // 输出 userName1
        System.out.println(obj);

        // #author 表示需要操作的对象不是 root 对象，而是 OgnlContext 中 key 为 author 对象
        obj = Ognl.getValue(Ognl.parseExpression("#author.username"), context, context.getRoot());

        // 输出 userName2
        System.out.println(obj);
    }


    @Test
    public void test2() throws OgnlException {

        // 调用 root 对象的author属性的 getEmail() 方法
        Object obj = Ognl.getValue("author.getEmail()", context, context.getRoot());
        // 输出 linhx@qq.com
        System.out.println(obj);

        // 调用 Blog.staticMethod() 静态方法
        obj = Ognl.getValue("@ognl.domain.Blog@staticMethod()", context, context.getRoot());
        // 输出 静态方法
        System.out.println(obj);

        // 访问 Blog.staticField 静态字段
        obj = Ognl.getValue("@ognl.domain.Blog@staticField", context, context.getRoot());
        // 输出 static Filed
        System.out.println(obj);
    }

    @Test
    public void test3() throws OgnlException {

        // 调用 root 对象的 posts 属性的 第一个 Post 对象
        Object obj = Ognl.getValue("posts[0]", context, context.getRoot());
        // 输出 true
        System.out.println(obj instanceof Post);
        // 输出 Post{content='postContent', author=Author{id=1, username='userName1', password='pwd1', email='linhx@qq.com'}}
        System.out.println(obj);

        Map<String, String> map = new HashMap<>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        context.put("map",map);
        // 访问非 root 对象集合
        obj = Ognl.getValue("#map['k2']", context, context.getRoot());
        // 输出 v2
        System.out.println(obj);
    }

    @Test
    public void test4() throws OgnlException {
        OgnlContext context = new OgnlContext(null, null, new OgnlMemberAccess());

        Author author = new Author();
        author.setId(12);
        context.setRoot(author);

        Object ognl = Ognl.parseExpression("username != null and username != ''");
        Object value = Ognl.getValue(ognl, context, context.getRoot());
        System.out.println("value1:"+value);// 输出 false

        author.setUsername("linhx");
        value = Ognl.getValue(ognl, context, context.getRoot());
        System.out.println("value2:"+value); // 输出 true

    }
    @Test
    public void test5() throws OgnlException {
        OgnlContext context = new OgnlContext(null, null, new OgnlMemberAccess());

        Author author = new Author();
        author.setId(12);
        context.setRoot(author);

        Object ognl = Ognl.parseExpression("@ognl.util.OgnlUtils@isEmpty(username)");
        Object value = Ognl.getValue(ognl, context, context.getRoot());
        System.out.println("value1:"+value);// 输出 true

        author.setUsername("linhx");
        value = Ognl.getValue(ognl, context, context.getRoot());
        System.out.println("value2:"+value); // 输出 false

    }

}
