/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.executor.resultset;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.ObjectTypeHandler;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.apache.ibatis.type.UnknownTypeHandler;

/**
 * 记录了 Result 中的一些元数据，并且提供了一系列操作 ResultSet 的辅组方法
 * @author Iwao AVE!
 */
public class ResultSetWrapper {

  /** 底层封装了 ResultSet 对象 */
  private final ResultSet resultSet;
  /** 全局的类型处理器注册中心 */
  private final TypeHandlerRegistry typeHandlerRegistry;
  /** 记录了 ResultSet 中每列的列名 */
  private final List<String> columnNames = new ArrayList<>();
  /** 记录了 ResultSet 中每列对应的 java 类型 */
  private final List<String> classNames = new ArrayList<>();
  /** 记录了 ResultSet 中每列对应的 jdbcType 类型 */
  private final List<JdbcType> jdbcTypes = new ArrayList<>();
  /**
   * 记录了每列对应的 TypeHandler 对象,
   * <pre>
   *     key：列名
   *     value：TypeHandler 集合
   * </pre>
   */
  private final Map<String, Map<Class<?>, TypeHandler<?>>> typeHandlerMap = new HashMap<>();
  /**
   * 记录了被映射的列名
   * <pre>
   *     key：ResultMap 对象的 id
   *     value：该 ResultMap 对象映射的列名集合
   * </pre>
   */
  private final Map<String, List<String>> mappedColumnNamesMap = new HashMap<>();
  /**
   * 记录了未被映射的列名
   * <pre>
   *     key：ResultMap 对象的 id
   *     value：该 ResultMap 对象未映射的列名集合
   * </pre>
   */
  private final Map<String, List<String>> unMappedColumnNamesMap = new HashMap<>();

  public ResultSetWrapper(ResultSet rs, Configuration configuration) throws SQLException {
    super();
    this.typeHandlerRegistry = configuration.getTypeHandlerRegistry();
    this.resultSet = rs;
    // 获取元数据对象
    final ResultSetMetaData metaData = rs.getMetaData();
    // 获取一行数据列的数量
    final int columnCount = metaData.getColumnCount();
    for (int i = 1; i <= columnCount; i++) {
      // metaData.getColumnLabel():获取用于打印输出和显示的指定列的建议标题（Sql "AS"语句指定的别名）
      // metaData.getColumnName(i)：获取指定列的名称
      // 获取“列名”或通过“AS”关键字指定的列名
      columnNames.add(configuration.isUseColumnLabel() ? metaData.getColumnLabel(i) : metaData.getColumnName(i));
      // 该列的 jdbcType 类型
      jdbcTypes.add(JdbcType.forCode(metaData.getColumnType(i)));
      // 获取对应的 Java 类型
      classNames.add(metaData.getColumnClassName(i));
    }
  }

  public ResultSet getResultSet() {
    return resultSet;
  }

  public List<String> getColumnNames() {
    return this.columnNames;
  }

  public List<String> getClassNames() {
    return Collections.unmodifiableList(classNames);
  }

  public List<JdbcType> getJdbcTypes() {
    return jdbcTypes;
  }

  /**
   * 获取指定列名的 jdbcType 类型
   * @param columnName 列名
   * @return jdbcType
   */
  public JdbcType getJdbcType(String columnName) {
    for (int i = 0 ; i < columnNames.size(); i++) {
      if (columnNames.get(i).equalsIgnoreCase(columnName)) {
        return jdbcTypes.get(i);
      }
    }
    return null;
  }

  /**
   * 获取 TypeHandler
   * <br/>
   * Gets the type handler to use when reading the result set.
   * Tries to get from the TypeHandlerRegistry by searching for the property type.
   * If not found it gets the column JDBC type and tries to get a handler for it.
   * 
   * @param propertyType 属性类型(javaType)
   * @param columnName 列名
   * @return TypeHandler
   */
  public TypeHandler<?> getTypeHandler(Class<?> propertyType, String columnName) {
    TypeHandler<?> handler = null;
    Map<Class<?>, TypeHandler<?>> columnHandlers = typeHandlerMap.get(columnName);
    if (columnHandlers == null) {
      columnHandlers = new HashMap<>();
      typeHandlerMap.put(columnName, columnHandlers);
    } else {
      handler = columnHandlers.get(propertyType);
    }
    if (handler == null) {
      // 获取 jdbcType 类型
      JdbcType jdbcType = getJdbcType(columnName);
      handler = typeHandlerRegistry.getTypeHandler(propertyType, jdbcType);
      // Replicate logic of UnknownTypeHandler#resolveTypeHandler
      // See issue #59 comment 10
      if (handler == null || handler instanceof UnknownTypeHandler) {
        final int index = columnNames.indexOf(columnName);
        final Class<?> javaType = resolveClass(classNames.get(index));
        if (javaType != null && jdbcType != null) {
          handler = typeHandlerRegistry.getTypeHandler(javaType, jdbcType);
        } else if (javaType != null) {
          handler = typeHandlerRegistry.getTypeHandler(javaType);
        } else if (jdbcType != null) {
          handler = typeHandlerRegistry.getTypeHandler(jdbcType);
        }
      }
      if (handler == null || handler instanceof UnknownTypeHandler) {
        handler = new ObjectTypeHandler();
      }
      columnHandlers.put(propertyType, handler);
    }
    return handler;
  }

  private Class<?> resolveClass(String className) {
    try {
      // #699 className could be null
      if (className != null) {
        return Resources.classForName(className);
      }
    } catch (ClassNotFoundException e) {
      // ignore
    }
    return null;
  }

  /**
   * 加载 ResultMap 后存入 mappedColumnNamesMap 集合中
   * @param resultMap 指定 ResultMap
   * @param columnPrefix 列前缀组成
   */
  private void loadMappedAndUnmappedColumnNames(ResultMap resultMap, String columnPrefix) throws SQLException {
    List<String> mappedColumnNames = new ArrayList<>();
    List<String> unmappedColumnNames = new ArrayList<>();
    final String upperColumnPrefix = columnPrefix == null ? null : columnPrefix.toUpperCase(Locale.ENGLISH);
    // ResultMap 中定义的列名加上前缀，得到实际映射的列名
    final Set<String> mappedColumns = prependPrefixes(resultMap.getMappedColumns(), upperColumnPrefix);
    for (String columnName : columnNames) {
      final String upperColumnName = columnName.toUpperCase(Locale.ENGLISH);
      if (mappedColumns.contains(upperColumnName)) {
        // 记录映射的列名
        mappedColumnNames.add(upperColumnName);
      } else {
        // 记录未映射的列名
        unmappedColumnNames.add(columnName);
      }
    }
    mappedColumnNamesMap.put(getMapKey(resultMap, columnPrefix), mappedColumnNames);
    unMappedColumnNamesMap.put(getMapKey(resultMap, columnPrefix), unmappedColumnNames);
  }

  /**
   * 获取该 ResultMap 中明确需要进行映射的列名集合
   * <pre>
   *     1 将 ResultMap 明确映射的列名集合
   *     2 未映射的列名集合记录到 mappedColumnNames 和 unmappedColumnNames 中缓存
   * </pre>
   *
   * @param resultMap 指定 ResultMap
   * @param columnPrefix 列前缀组成
   * @return 指定 ResultMap 对象中明确映射的列名集合
   */
  public List<String> getMappedColumnNames(ResultMap resultMap, String columnPrefix) throws SQLException {
    // 在 mappedColumnNamesMap 集合中查找被映射的列名（key 是由 ResultMap 的 id 与 列前缀组成）
    List<String> mappedColumnNames = mappedColumnNamesMap.get(getMapKey(resultMap, columnPrefix));
    if (mappedColumnNames == null) {
      // 未查找到指定 ResultMap 映射的列名，则加载后存入 mappedColumnNamesMap 集合中
      loadMappedAndUnmappedColumnNames(resultMap, columnPrefix);
      mappedColumnNames = mappedColumnNamesMap.get(getMapKey(resultMap, columnPrefix));
    }
    return mappedColumnNames;
  }

  /**
   * 获取未被映射的列名
   */
  public List<String> getUnmappedColumnNames(ResultMap resultMap, String columnPrefix) throws SQLException {
    List<String> unMappedColumnNames = unMappedColumnNamesMap.get(getMapKey(resultMap, columnPrefix));
    if (unMappedColumnNames == null) {
      loadMappedAndUnmappedColumnNames(resultMap, columnPrefix);
      unMappedColumnNames = unMappedColumnNamesMap.get(getMapKey(resultMap, columnPrefix));
    }
    return unMappedColumnNames;
  }

  /**
   * 该类的 Map 的 key值
   * @param resultMap 指定 ResultMap
   * @param columnPrefix 列前缀组成
   * @return resultMap.getId() + ":" + columnPrefix;
   */
  private String getMapKey(ResultMap resultMap, String columnPrefix) {
    return resultMap.getId() + ":" + columnPrefix;
  }

  /**
   *  ResultMap 中定义的列名加上前缀，得到实际映射的列名
   * @param columnNames ResultMap 映射的列名
   * @param prefix 列前缀
   * @return prefix+columnName 集合
   */
  private Set<String> prependPrefixes(Set<String> columnNames, String prefix) {
    if (columnNames == null || columnNames.isEmpty() || prefix == null || prefix.length() == 0) {
      return columnNames;
    }
    final Set<String> prefixed = new HashSet<>();
    for (String columnName : columnNames) {
      prefixed.add(prefix + columnName);
    }
    return prefixed;
  }
  
}
