/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.executor.loader;

import java.util.List;
import java.util.Properties;

import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.session.Configuration;

/**
 * @author Eduardo Macarron
 */
public interface ProxyFactory {

  /**
   * 根据配置初始化 ProxyFactory 对象
   * <pre>
   *     MyBatis 提供的两个子类中，该方法都是空实现
   * </pre>
   * @param properties
   */
  void setProperties(Properties properties);

  /**
   * 创建代理
   * @param target 代理的目标对象
   * @param lazyLoader 延迟加载的属性名称和对应 ResultLoader 对象之间的关系
   * @param configuration 全局配置信息
   * @param objectFactory 工厂类
   * @param constructorArgTypes 构造方法的参数类型
   * @param constructorArgs 构造方法的参数值
   * @return 代理类
   */
  Object createProxy(Object target, ResultLoaderMap lazyLoader, Configuration configuration, ObjectFactory objectFactory, List<Class<?>> constructorArgTypes, List<Object> constructorArgs);
  
}
