/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.executor.parameter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 负责对用户传递的参数转换成JDBC Statement 所需要的参数
 * <br/>
 * <pre>
 *     功能：为SQL语句绑定实参，
 *     也就是使用传入的实参替换 SQL 语句中 “?”占位符
 * </pre>
 * A parameter handler sets the parameters of the {@code PreparedStatement}
 *
 * @author Clinton Begin
 */
public interface ParameterHandler {

  Object getParameterObject();

  /**
   * 主要负责调用 PreParedStatement.set*()方法为SQL语句绑定实参。
   * @param ps
   * @throws SQLException
   */
  void setParameters(PreparedStatement ps)
      throws SQLException;

}
