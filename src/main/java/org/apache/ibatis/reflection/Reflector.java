/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.reflection;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.ReflectPermission;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.ibatis.reflection.invoker.GetFieldInvoker;
import org.apache.ibatis.reflection.invoker.Invoker;
import org.apache.ibatis.reflection.invoker.MethodInvoker;
import org.apache.ibatis.reflection.invoker.SetFieldInvoker;
import org.apache.ibatis.reflection.property.PropertyNamer;

/**
 * 记录某类相关的元信息<br/>
 * This class represents a cached set of class definition information that
 * allows for easy mapping between property names and getter/setter methods.
 *
 * @author Clinton Begin
 */
public class Reflector {

  /** 对应的Class类型 */
  private final Class<?> type;
  /**
   * 可读属性的名称集合
   * <pre>
   *     可读属性就是存在相应getter方法的属性
   *     初始值为空数组
   * </pre>
   */
  private final String[] readablePropertyNames;
  /**
   * 可写属性的名称集合
   * <pre>
   *     可写属性就是存在相应setter方法的属性
   *     初始值为空数组
   * </pre>
   */
  private final String[] writeablePropertyNames;
  /**
   * 记录了属性相应的setter方法
   * <pre>
   *     key是属性名称，value是Invoker对象
   *     Invoker是对setter方法对应Method对象的封装
   * </pre>
   */
  private final Map<String, Invoker> setMethods = new HashMap<>();
  /**
   * 记录了属性相应的getter方法
   * <pre>
   *     key是属性名称，value是Invoker对象
   *     Invoker是对getter方法对应Method对象的封装
   * </pre>
   */
  private final Map<String, Invoker> getMethods = new HashMap<>();
  /**
   * 记录了属性相应的setter方法的参数值类型
   * <pre>
   *     key是属性名称，value是setter方法的参数类型
   * </pre>
   */
  private final Map<String, Class<?>> setTypes = new HashMap<>();
  /**
   * 记录了属性相应的getter方法的返回值类型
   * <pre>
   *     key是属性名称，value是getter方法的参数类型
   * </pre>
   */
  private final Map<String, Class<?>> getTypes = new HashMap<>();
  /** 记录了默认构造方法 */
  private Constructor<?> defaultConstructor;
  /**
   * 记录了所有属性名称的集合，并将属性的大写格式录入 Key
   */
  private Map<String, String> caseInsensitivePropertyMap = new HashMap<>();

  public Reflector(Class<?> clazz) {
    // 初始化type字段
    type = clazz;
    // 查找clazz的默认构造方法(无参构造方法)
    addDefaultConstructor(clazz);
    // 处理clazz中的getter方法，填充getMethods集合和getTypes集合
    addGetMethods(clazz);
    // 处理clazz中的setter方法，填充setMethods集合和getTypes集合
    addSetMethods(clazz);
    // 处理没有getter/setter的字段
    addFields(clazz);
    // 根据getMethods集合，初始化可读属性的名称集合
    readablePropertyNames = getMethods.keySet().toArray(new String[getMethods.keySet().size()]);
    // 根据setMethods集合，初始化可写属性的名称集合
    writeablePropertyNames = setMethods.keySet().toArray(new String[setMethods.keySet().size()]);
    // 初始化caseInsensitivePropertyMap集合，记录了所有大写格式的属性名称
    for (String propName : readablePropertyNames) {
      // 参数名大写为key,原型为value
      caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
    }
    for (String propName : writeablePropertyNames) {
      // 参数名大写为key,原型为value
      caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
    }
  }

  /**
   * 查找clazz的默认构造方法(无参构造方法)
   * <pre>
   *     实现方法：是通过反射遍历所有的构造方法
   * </pre>
   * @param clazz 对应的Class类型
   */
  private void addDefaultConstructor(Class<?> clazz) {
    // 获取构造函数
    Constructor<?>[] consts = clazz.getDeclaredConstructors();
    for (Constructor<?> constructor : consts) {
      if (constructor.getParameterTypes().length == 0) {
        // 没有参数的构造函数，进行赋值
        this.defaultConstructor = constructor;
      }
    }
  }

  /**
   * 解析类中定义的getter方法
   * <pre>
   *     1. 获取指定'类'以及其'父类'和'接口'中定义的方法
   *     2. 安装 javaBean 规范查找 getter 方法，并记录到 conflictingGetter 集合中
   *     3. 对 conflictingGetter 集合进行处理
   * </pre>
   */
  private void addGetMethods(Class<?> cls) {
    // conflictingGetters 集合的 key 为属性名称，value是相应 getter 方法集合。
    // 因为子类可能覆盖父类的 getter,所以同一个属性名称会存在多个 getter 方法
    Map<String, List<Method>> conflictingGetters = new HashMap<>();
    // 1. 获取指定'类'以及其'父类'和'接口'中定义的方法
    Method[] methods = getClassMethods(cls);
    // 2. 安装 javaBean 规范查找 getter 方法，并记录到 conflictingGetter 集合中
    for (Method method : methods) {
      if (method.getParameterTypes().length > 0) {
        // 方法的参数列表不是空的(既该方法是有参数)，不是 getter 方法。
        // 该方法进行忽略
        continue;
      }
      // 方法名
      String name = method.getName();
      /*
        JavaBean 中 getter 方法的方法名以‘get’开头，长度大于3
                        以‘is’开头，长度必须大于2。
       */
      if ((name.startsWith("get") && name.length() > 3)
          || (name.startsWith("is") && name.length() > 2)) {
        name = PropertyNamer.methodToProperty(name);
        // 将属性名与 getter 方法的对应关系记录到 conflictingGetters 集合中
        addMethodConflict(conflictingGetters, name, method);
      }
    }
    // 解决 getter 冲突
    resolveGetterConflicts(conflictingGetters);
  }

  /**
   * 解决 getter 冲突
   * <pre>
   *   对冲突的 getter 进行处理，并记录到getMethods
   * </pre>
   * @param conflictingGetters getter的集合
   */
  private void resolveGetterConflicts(Map<String, List<Method>> conflictingGetters) {
    // 遍历 conflictingGetters 集合
    for (Entry<String, List<Method>> entry : conflictingGetters.entrySet()) {
      Method winner = null;
      String propName = entry.getKey();

      for (Method candidate : entry.getValue()) {
        if (winner == null) {
          winner = candidate;
          continue;
        }
        /*
         * 同一个属性名称存在多个getter方法，则需要比较这些 getter 方法的返回值。
         * 选择 getter 方法迭代过程中的临时变量，用于记录迭代到目前为止。
         */
        // 获取返回值类型
        Class<?> winnerType = winner.getReturnType();
        Class<?> candidateType = candidate.getReturnType();
        if (candidateType.equals(winnerType)) {
          // 返回值相同
          if (!boolean.class.equals(candidateType)) {
            // 返回类型相同，且不是 boolean 类型，应该在第一步骤过滤，到这里只能抛异常
            throw new ReflectionException(
                "Illegal overloaded getter method with ambiguous type for property "
                    + propName + " in class " + winner.getDeclaringClass()
                    + ". This breaks the JavaBeans specification and can cause unpredictable results.");
          } else if (candidate.getName().startsWith("is")) {
            winner = candidate;
          }
        } else if (candidateType.isAssignableFrom(winnerType)) {
          // 当前最合适(winner)方法的返回值，是当前(candidate)方法返回值的子类，什么都不用做。
          // 当前 winnerType 方法是最合适的方法
          // OK getter type is descendant
        } else if (winnerType.isAssignableFrom(candidateType)) {
          // 当前方法(candidateType)的返回值是当前最合适(winner)方法的返回值的子类，更新临时变量 getter,
          // candidate方法成为最合适的 getter 方法
          winner = candidate;
        } else {
          // 返回值相同，二仪性，抛出异常
          throw new ReflectionException(
              "Illegal overloaded getter method with ambiguous type for property "
                  + propName + " in class " + winner.getDeclaringClass()
                  + ". This breaks the JavaBeans specification and can cause unpredictable results.");
        }
      }
      // 添加到 getMethod 集合并填充getTypes 集合
      addGetMethod(propName, winner);
    }
  }

  /**
   * 添加到 getMethod 集合并填充getTypes 集合
   * @param name getter名称
   * @param method getter方法
   */
  private void addGetMethod(String name, Method method) {
    // 检测属性是否合法
    if (isValidPropertyName(name)) {
      // 将属性名以及对应的 MethodInvoker 对象添加到 getMethods 集合中
      getMethods.put(name, new MethodInvoker(method));
      // 获取返回值Type
      Type returnType = TypeParameterResolver.resolveReturnType(method, type);
      // 将属性名称及其 getter 方法的返回值类型添加到 getTypes 集合中保存
      getTypes.put(name, typeToClass(returnType));
    }
  }

  /**
   * 添加该类的 setter方法
   */
  private void addSetMethods(Class<?> cls) {
    // conflictingGetters 集合的 key 为属性名称，value是相应 setter 方法集合。
    // 因为子类可能覆盖父类的 setter,所以同一个属性名称会存在多个 setter 方法
    Map<String, List<Method>> conflictingSetters = new HashMap<>();
    // 获取该类的所有方法
    Method[] methods = getClassMethods(cls);
    for (Method method : methods) {
      // 获取方法名
      String name = method.getName();
      if (name.startsWith("set") && name.length() > 3) {
        // 方法的参数 有且只有一个
        if (method.getParameterTypes().length == 1) {
          // 获取去除掉前缀 set 的方法名
          name = PropertyNamer.methodToProperty(name);
          // 将set方法的信息 加载到 conflictingSetter
          addMethodConflict(conflictingSetters, name, method);
        }
      }
    }
    // 解决 setter 方法冲突
    resolveSetterConflicts(conflictingSetters);
  }

  /**
   * 将方法的信息加载到 conflictingMethods
   * @param conflictingMethods Map<String>,List<Method>> 封装的信息
   * @param name 方法名 -> key
   * @param method 方法 -> value
   */
  private void addMethodConflict(Map<String, List<Method>> conflictingMethods, String name, Method method) {
    List<Method> list = conflictingMethods.computeIfAbsent(name, k -> new ArrayList<>());
    list.add(method);
  }

  /**
   * 解决 conflictingSetters 里面冲突的方法
   * @param conflictingSetters 需要解决的信息
   */
  private void resolveSetterConflicts(Map<String, List<Method>> conflictingSetters) {
    // 根据 key 对 conflictingSetters 进行遍历
    for (String propName : conflictingSetters.keySet()) {
      // 获取该方法名的 所有方法
      List<Method> setters = conflictingSetters.get(propName);
      // 获取该 propName 字段 get的返回类型
      Class<?> getterType = getTypes.get(propName);
      Method match = null;
      ReflectionException exception = null;
      for (Method setter : setters) {
        // 获取该方法第一个参数的类型
        Class<?> paramType = setter.getParameterTypes()[0];
        // 参数类型和 getter 的返回类型一致，赋值到 match 并退出该方法循环
        if (paramType.equals(getterType)) {
          // should be the best match
          match = setter;
          break;
        }
        if (exception == null) {
          try {
            // 对这个两个方法 选择一个 重载的方法
            match = pickBetterSetter(match, setter, propName);
          } catch (ReflectionException e) {
            // there could still be the 'best match'
            match = null;
            exception = e;
          }
        }
      }
      // 遍历所有方法都没选到最优的方法，抛ReflectionException异常
      if (match == null) {
        throw exception;
      } else {
        // 添加 setter 方法
        addSetMethod(propName, match);
      }
    }
  }

  /**
   * 比较两个方法，获取最优的方法
   * @param setter1 set方法1
   * @param setter2 set方法2
   * @param property 参数
   * @return 返回最好方法
   */
  private Method pickBetterSetter(Method setter1, Method setter2, String property) {
    if (setter1 == null) {
      return setter2;
    }
    // 方法1的第一个参数类型
    Class<?> paramType1 = setter1.getParameterTypes()[0];
    // 方法2的第一个参数类型
    Class<?> paramType2 = setter2.getParameterTypes()[0];
    if (paramType1.isAssignableFrom(paramType2)) {
      // 方法2是方法1的重载
      return setter2;
    } else if (paramType2.isAssignableFrom(paramType1)) {
      // 方法1是方法2的重载
      return setter1;
    }
    throw new ReflectionException("Ambiguous setters defined for property '" + property + "' in class '"
        + setter2.getDeclaringClass() + "' with types '" + paramType1.getName() + "' and '"
        + paramType2.getName() + "'.");
  }

  /**
   * 将方法添加到SetMethod里面
   * @param name 参数名
   * @param method set方法
   */
  private void addSetMethod(String name, Method method) {
    // 名字的合法性
    if (isValidPropertyName(name)) {
      // 将参数 -> key, setter方法 -> value,录入到setMethods
      setMethods.put(name, new MethodInvoker(method));
      // 获取该方法的参数类型列表
      Type[] paramTypes = TypeParameterResolver.resolveParamTypes(method, type);
      // set方法名去除set前缀的方法名 -> key;set方法的第一个参数的类型 -> value
      setTypes.put(name, typeToClass(paramTypes[0]));
    }
  }

  /**
   * 将 type 转换成 class 对象
   * @param src 需要转 class 对象的参数
   * @return 返回该参数的 class 对象
   */
  private Class<?> typeToClass(Type src) {
    Class<?> result = null;
    if (src instanceof Class) {
      result = (Class<?>) src;
    } else if (src instanceof ParameterizedType) {
      result = (Class<?>) ((ParameterizedType) src).getRawType();
    } else if (src instanceof GenericArrayType) {
      Type componentType = ((GenericArrayType) src).getGenericComponentType();
      if (componentType instanceof Class) {
        result = Array.newInstance((Class<?>) componentType, 0).getClass();
      } else {
        Class<?> componentClass = typeToClass(componentType);
        result = Array.newInstance(componentClass, 0).getClass();
      }
    }
    if (result == null) {
      // 找不出 class 对象，将用 Object的class 对象
      result = Object.class;
    }
    return result;
  }

  /**
   * 处理类中定义的所有字段
   * <pre>
   *     将处理后的字段信息添加到
   * </pre>
   * @param clazz 字段所属的类
   */
  private void addFields(Class<?> clazz) {
    // 获取clazz中定义的全部字段
    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
      // 当 setMethods 集合不包含同名属性时，将其记录到 setMethods 集合和 setType 集合中
      if (!setMethods.containsKey(field.getName())) {
        // issue #379 - removed the check for final because JDK 1.5 allows
        // modification of final fields through reflection (JSR-133). (JGB)
        // pr #16 - final static can only be set by the classloader
        // 获取该字段的修饰类型。public String name，name字段的 modifiers 返回"public String".
        int modifiers = field.getModifiers();
        // 过滤掉 final 和 static 的修饰字段
        if (!(Modifier.isFinal(modifiers) && Modifier.isStatic(modifiers))) {
          // 将其记录到 setMethods 集合和 setType 集合中
          addSetField(field);
        }
      }
      // 当 getMethods 集合不包含同名属性时，将其记录到 getMethods 集合和 getType 集合中
      if (!getMethods.containsKey(field.getName())) {
        // 将其记录到 getMethods 集合和 getType 集合中
        addGetField(field);
      }
    }
    if (clazz.getSuperclass() != null) {
      // 处理父类中字段
      addFields(clazz.getSuperclass());
    }
  }

  /**
   * 将属性添加到 setMethods和setTypes
   * @param field 该类的某个属性
   */
  private void addSetField(Field field) {
    // 校验该字段名称是否合法
    if (isValidPropertyName(field.getName())) {
      setMethods.put(field.getName(), new SetFieldInvoker(field));
      Type fieldType = TypeParameterResolver.resolveFieldType(field, type);
      setTypes.put(field.getName(), typeToClass(fieldType));
    }
  }

  /**
   * 将属性添加到 getMethods和getTypes
   * @param field 该类的某个属性
   */
  private void addGetField(Field field) {
    if (isValidPropertyName(field.getName())) {
      getMethods.put(field.getName(), new GetFieldInvoker(field));
      Type fieldType = TypeParameterResolver.resolveFieldType(field, type);
      getTypes.put(field.getName(), typeToClass(fieldType));
    }
  }

  /**
   * 检测参数的名字是否合法
   * @param name 参数名
   * @return true-合法，false-不合法
   */
  private boolean isValidPropertyName(String name) {
    return !(name.startsWith("$") || "serialVersionUID".equals(name) || "class".equals(name));
  }

  /**
   * 获取这个{@code cls}类的所有方法，包括父类和接口
   * <pre>
   *    获取这个类和所有的父类，接口中声明的方法
   *    我们使用这个方法，而不是更简单的Class.getMethods()，
   *    因为我们也想寻找私有方法。
   * </pre>
   *
   * This method returns an array containing all methods
   * declared in this class and any superclass.
   * We use this method, instead of the simpler Class.getMethods(),
   * because we want to look for private methods as well.
   *
   * @param cls The class
   * @return An array containing all methods in this class
   */
  private Method[] getClassMethods(Class<?> cls) {
    // 记录指定类中的全部方法的唯一签名以及对应的Method对象
    Map<String, Method> uniqueMethods = new HashMap<>();
    Class<?> currentClass = cls;
    while (currentClass != null && currentClass != Object.class) {
      // 记录 currentClass 这个类中定义的全部方法
      addUniqueMethods(uniqueMethods, currentClass.getDeclaredMethods());

      // we also need to look for interface methods -
      // because the class may be abstract
      // 获取该类的所有接口类
      Class<?>[] interfaces = currentClass.getInterfaces();
      for (Class<?> anInterface : interfaces) {
        // 记录 interfaces 这个接口中定义的全部方法
        addUniqueMethods(uniqueMethods, anInterface.getMethods());
      }
      // 获取父类，继续while循环
      currentClass = currentClass.getSuperclass();
    }

    Collection<Method> methods = uniqueMethods.values();
    // 转换成 Methods 数组并返回
    return methods.toArray(new Method[methods.size()]);
  }

  /**
   * <pre>
   *     为 method 的所有方法生成唯一的签名，并记录到 uniqueMethods
   * </pre>
   * @param uniqueMethods 记录每个方法的HastMap
   * @param methods 方法集合
   */
  private void addUniqueMethods(Map<String, Method> uniqueMethods, Method[] methods) {
    for (Method currentMethod : methods) {
      // 该方法是否是桥接
      if (!currentMethod.isBridge()) {
        // 获得唯一的签名
        String signature = getSignature(currentMethod);
        // check to see if the method is already known
        // if it is known, then an extended class must have
        // overridden a method
        // 检测是否在子类中已经添加过该方法，如果在子类中已经添加，则表示子类覆盖了该方法。
        // 无须再向 uniqueMethods 集合中添加该方法
        if (!uniqueMethods.containsKey(signature)) {
          uniqueMethods.put(signature, currentMethod);
        }
      }
    }
  }

  /**
   * 方法签名
   * <pre>
   *  为方法生成一个方法签名。
   *  该签名是全局唯一的，可作为该方法的唯一标识
   *  得到的方法签名是：<code>返回值类型#方法名称:参数类型列表</code>
   *    Eg:getSignature(method) 签名是：Java.lang.String#getSignature:java.lang.reflect.Mathod
   * </pre>
   * @param method 方法
   * @return 参数类型列表
   */
  private String getSignature(Method method) {
    StringBuilder sb = new StringBuilder();
    Class<?> returnType = method.getReturnType();
    if (returnType != null) {
      sb.append(returnType.getName()).append('#');
    }
    sb.append(method.getName());
    Class<?>[] parameters = method.getParameterTypes();
    for (int i = 0; i < parameters.length; i++) {
      if (i == 0) {
        sb.append(':');
      } else {
        sb.append(',');
      }
      sb.append(parameters[i].getName());
    }
    return sb.toString();
  }

  /**
   * 检测是否可以控制可访问的属性<br/>
   * Checks whether can control member accessible.
   *
   * @return If can control member accessible, it return {@literal true}
   * @since 3.5.0
   */
  public static boolean canControlMemberAccessible() {
    try {
      SecurityManager securityManager = System.getSecurityManager();
      if (null != securityManager) {
        securityManager.checkPermission(new ReflectPermission("suppressAccessChecks"));
      }
    } catch (SecurityException e) {
      return false;
    }
    return true;
  }

  /**
   * Gets the name of the class the instance provides information for
   *
   * @return The class name
   */
  public Class<?> getType() {
    return type;
  }

  /**
   * 获取该类的某人构造方法
   */
  public Constructor<?> getDefaultConstructor() {
    if (defaultConstructor != null) {
      return defaultConstructor;
    } else {
      throw new ReflectionException("There is no default constructor for " + type);
    }
  }

  public boolean hasDefaultConstructor() {
    return defaultConstructor != null;
  }

  public Invoker getSetInvoker(String propertyName) {
    Invoker method = setMethods.get(propertyName);
    if (method == null) {
      throw new ReflectionException("There is no setter for property named '" + propertyName + "' in '" + type + "'");
    }
    return method;
  }

  public Invoker getGetInvoker(String propertyName) {
    Invoker method = getMethods.get(propertyName);
    if (method == null) {
      throw new ReflectionException("There is no getter for property named '" + propertyName + "' in '" + type + "'");
    }
    return method;
  }

  /**
   * Gets the type for a property setter
   *
   * @param propertyName - the name of the property
   * @return The Class of the property setter
   */
  public Class<?> getSetterType(String propertyName) {
    Class<?> clazz = setTypes.get(propertyName);
    if (clazz == null) {
      throw new ReflectionException("There is no setter for property named '" + propertyName + "' in '" + type + "'");
    }
    return clazz;
  }

  /**
   * Gets the type for a property getter
   *
   * @param propertyName - the name of the property
   * @return The Class of the property getter
   */
  public Class<?> getGetterType(String propertyName) {
    Class<?> clazz = getTypes.get(propertyName);
    if (clazz == null) {
      throw new ReflectionException("There is no getter for property named '" + propertyName + "' in '" + type + "'");
    }
    return clazz;
  }

  /**
   * Gets an array of the readable properties for an object
   *
   * @return The array
   */
  public String[] getGetablePropertyNames() {
    return readablePropertyNames;
  }

  /**
   * Gets an array of the writable properties for an object
   *
   * @return The array
   */
  public String[] getSetablePropertyNames() {
    return writeablePropertyNames;
  }

  /**
   * Check to see if a class has a writable property by name
   *
   * @param propertyName - the name of the property to check
   * @return True if the object has a writable property by the name
   */
  public boolean hasSetter(String propertyName) {
    return setMethods.keySet().contains(propertyName);
  }

  /**
   * Check to see if a class has a readable property by name
   *
   * @param propertyName - the name of the property to check
   * @return True if the object has a readable property by the name
   */
  public boolean hasGetter(String propertyName) {
    return getMethods.keySet().contains(propertyName);
  }

  /**
   * 查找该类的属性名称
   * @param name 属性名
   * @return 属性的字段名
   */
  public String findPropertyName(String name) {
    return caseInsensitivePropertyMap.get(name.toUpperCase(Locale.ENGLISH));
  }
}
