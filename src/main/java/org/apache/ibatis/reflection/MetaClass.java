/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import org.apache.ibatis.reflection.invoker.GetFieldInvoker;
import org.apache.ibatis.reflection.invoker.Invoker;
import org.apache.ibatis.reflection.invoker.MethodInvoker;
import org.apache.ibatis.reflection.property.PropertyTokenizer;

/**
 * <pre>
 *  解析复杂的属性表达式
 *  获取指定属性的描述信息
 * </pre>
 * @author Clinton Begin
 */
public class MetaClass {

  /** ReflectorFactory 用户缓存 Reflector 对象 */
  private final ReflectorFactory reflectorFactory;
  /** Reflector 用于记录该类相关信息 */
  private final Reflector reflector;

  private MetaClass(Class<?> type, ReflectorFactory reflectorFactory) {
    this.reflectorFactory = reflectorFactory;
    // 创建 Reflector 对象
    this.reflector = reflectorFactory.findForClass(type);
  }

  /**
   * 创建 MetaClass
   * @param type Class 对象
   * @param reflectorFactory 用户缓存 Reflector 对象
   * @return metaClass
   */
  public static MetaClass forClass(Class<?> type, ReflectorFactory reflectorFactory) {
    return new MetaClass(type, reflectorFactory);
  }

  /**
   * 根据属性名创建对应的 MetaClass
   * @param name 在reflector里面的属性名
   * @return MetaClass
   */
  public MetaClass metaClassForProperty(String name) {
    // 根据属性名查找该属性名的类型
    Class<?> propType = reflector.getGetterType(name);
    return MetaClass.forClass(propType, reflectorFactory);
  }

  /**
   * 查找属性
   * <pre>
   *  通过调用MetaClass.buildProperty()方法，里的PropertyTokenizer解析复杂的属性表达式
   * </pre>
   * @param name
   * @return
   */
  public String findProperty(String name) {
    // 委托给buildProperty()方法实现
    StringBuilder prop = buildProperty(name, new StringBuilder());
    return prop.length() > 0 ? prop.toString() : null;
  }

  public String findProperty(String name, boolean useCamelCaseMapping) {
    if (useCamelCaseMapping) {
      name = name.replace("_", "");
    }
    return findProperty(name);
  }

  public String[] getGetterNames() {
    return reflector.getGetablePropertyNames();
  }

  public String[] getSetterNames() {
    return reflector.getSetablePropertyNames();
  }

  public Class<?> getSetterType(String name) {
    PropertyTokenizer prop = new PropertyTokenizer(name);
    if (prop.hasNext()) {
      MetaClass metaProp = metaClassForProperty(prop.getName());
      return metaProp.getSetterType(prop.getChildren());
    } else {
      return reflector.getSetterType(prop.getName());
    }
  }

  public Class<?> getGetterType(String name) {
    PropertyTokenizer prop = new PropertyTokenizer(name);
    if (prop.hasNext()) {
      MetaClass metaProp = metaClassForProperty(prop);
      return metaProp.getGetterType(prop.getChildren());
    }
    // issue #506. Resolve the type inside a Collection Object
    return getGetterType(prop);
  }

  /**
   * 根据对应的属性的PropertyTokenizer 创建MetaClass
   * @param prop 属性的PropertyTokenizer
   * @return MetaClass
   */
  private MetaClass metaClassForProperty(PropertyTokenizer prop) {
    // 获取表达式所表示的属性类型
    Class<?> propType = getGetterType(prop);
    return MetaClass.forClass(propType, reflectorFactory);
  }

  /**
   * 获取表达式所表示的属性类型
   * @param prop 属性的PropertyTokenizer
   * @return 属性对应的Class对象
   */
  private Class<?> getGetterType(PropertyTokenizer prop) {
    // 获取属性类型
    Class<?> type = reflector.getGetterType(prop.getName());
    // 该表达式中是否使用“[]"指定下标，且是 Collection 子类
    if (prop.getIndex() != null && Collection.class.isAssignableFrom(type)) {
      // 通过 TypeParameterResolver 工具类解析属性的类型
      Type returnType = getGenericGetterType(prop.getName());
      if (returnType instanceof ParameterizedType) {
        // 获取实际的类型参数
        Type[] actualTypeArguments = ((ParameterizedType) returnType).getActualTypeArguments();
        if (actualTypeArguments != null && actualTypeArguments.length == 1) {
          // 泛型的类型
          returnType = actualTypeArguments[0];
          if (returnType instanceof Class) {
            type = (Class<?>) returnType;
          } else if (returnType instanceof ParameterizedType) {
            type = (Class<?>) ((ParameterizedType) returnType).getRawType();
          }
        }
      }
    }
    return type;
  }

  /**
   * 获取 property 的类型
   * @param propertyName 字段名
   * @return 类型
   */
  private Type getGenericGetterType(String propertyName) {
    try {
      /* 根据 Reflector.getMethods 集合中记录的 Invoker 实现类的类型，
          决定解析 getter 方法返回值类型 还是 解析字段类型
       */
      Invoker invoker = reflector.getGetInvoker(propertyName);
      if (invoker instanceof MethodInvoker) {
        // 解析 getter 方法
        Field _method = MethodInvoker.class.getDeclaredField("method");
        _method.setAccessible(true);
        Method method = (Method) _method.get(invoker);
        // 返回 getter 的返回值
        return TypeParameterResolver.resolveReturnType(method, reflector.getType());
      } else if (invoker instanceof GetFieldInvoker) {
        // 解析字段类型
        Field _field = GetFieldInvoker.class.getDeclaredField("field");
        _field.setAccessible(true);
        Field field = (Field) _field.get(invoker);
        // 返回字段类型
        return TypeParameterResolver.resolveFieldType(field, reflector.getType());
      }
    } catch (NoSuchFieldException | IllegalAccessException ignored) {
    }
    return null;
  }

  /**
   * 该字段 name 是否有 setter 方法
   * @param name 字段名
   * @return 是否有 setter 方法
   */
  public boolean hasSetter(String name) {
    PropertyTokenizer prop = new PropertyTokenizer(name);
    if (prop.hasNext()) {
      if (reflector.hasSetter(prop.getName())) {
        MetaClass metaProp = metaClassForProperty(prop.getName());
        return metaProp.hasSetter(prop.getChildren());
      } else {
        return false;
      }
    } else {
      return reflector.hasSetter(prop.getName());
    }
  }

  /**
   * 判断属性表达式所表示的属性是否有对应的get方法
   * @param name 属性名称
   * @return 是否有对应的get方法
   */
  public boolean hasGetter(String name) {
    // 解析属性表达式
    PropertyTokenizer prop = new PropertyTokenizer(name);
    // 存在待处理的子表达式
    if (prop.hasNext()) {
      // PropertyTokenizer.name 指定的属性有 getter 方法，才能处理子表达式
      if (reflector.hasGetter(prop.getName())) {
        MetaClass metaProp = metaClassForProperty(prop);
        // 递归入口
        return metaProp.hasGetter(prop.getChildren());
      } else {
        // 递归出口
        return false;
      }
    } else {
      // 递归出口
      return reflector.hasGetter(prop.getName());
    }
  }

  public Invoker getGetInvoker(String name) {
    return reflector.getGetInvoker(name);
  }

  public Invoker getSetInvoker(String name) {
    return reflector.getSetInvoker(name);
  }

  /**
   * 解析 name 的导航信息
   * @param name 属性民
   * @param builder 解析结果的封装类
   * @return builder
   */
  private StringBuilder buildProperty(String name, StringBuilder builder) {
    // 解析属性表达式
    PropertyTokenizer prop = new PropertyTokenizer(name);
    // 是否有子表达式
    if (prop.hasNext()) {
      // 查找 PropertyTokenizer.name对应的属性
      String propertyName = reflector.findPropertyName(prop.getName());
      if (propertyName != null) {
        // 追加属性名
        builder.append(propertyName);
        builder.append(".");
        // 为该属性创建对应的 MetaClass 对象
        MetaClass metaProp = metaClassForProperty(propertyName);
        // 递归解析 PropertyTokenizer.children 字段，并将解析结果添加到 build 中保存
        metaProp.buildProperty(prop.getChildren(), builder);
      }
    } else {
      // 递归出口
      // 查找 PropertyTokenizer.name对应的属性
      String propertyName = reflector.findPropertyName(name);
      if (propertyName != null) {
        // 追加属性名
        builder.append(propertyName);
      }
    }
    return builder;
  }

  public boolean hasDefaultConstructor() {
    return reflector.hasDefaultConstructor();
  }
}