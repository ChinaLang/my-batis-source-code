/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.scripting.xmltags;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.builder.BaseBuilder;
import org.apache.ibatis.builder.BuilderException;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.defaults.RawSqlSource;
import org.apache.ibatis.session.Configuration;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 创建SqlSource 是由 XMLScriptBuilder(Xml脚本构建) 对 select 等CRUD标签中的 sql 脚本进行处理
 * @author Clinton Begin
 */
public class XMLScriptBuilder extends BaseBuilder {

	/** [select/insert/update/delete] 节点信息  */
	private final XNode context;
	/** 标志动态 SQL 语句，true-是动态语句，false-否 */
	private boolean isDynamic;
	/** 入参对应的Java类型 */
	private final Class<?> parameterType;
	/** 各节点(trim/where/set/foreach等)的处理类型 */
	private final Map<String, NodeHandler> nodeHandlerMap = new HashMap<>();

	public XMLScriptBuilder(Configuration configuration, XNode context) {
		this(configuration, context, null);
	}

	public XMLScriptBuilder(Configuration configuration, XNode context, Class<?> parameterType) {
		super(configuration);
		this.context = context;
		this.parameterType = parameterType;
		// 初始化动态SQL中的节点处理器集合
		initNodeHandlerMap();
	}

	private void initNodeHandlerMap() {
		nodeHandlerMap.put("trim", new TrimHandler());
		nodeHandlerMap.put("where", new WhereHandler());
		nodeHandlerMap.put("set", new SetHandler());
		nodeHandlerMap.put("foreach", new ForEachHandler());
		nodeHandlerMap.put("if", new IfHandler());
		nodeHandlerMap.put("choose", new ChooseHandler());
		nodeHandlerMap.put("when", new IfHandler());
		nodeHandlerMap.put("otherwise", new OtherwiseHandler());
		nodeHandlerMap.put("bind", new BindHandler());
	}

	public SqlSource parseScriptNode() {
		// 获取解析过的SQL信息（解析了动态SQL标签和${}）
		// 注意：此时SQL语句中还有#{}没有处理
		MixedSqlNode rootSqlNode = parseDynamicTags(context);
		SqlSource sqlSource = null;
		// 如果包含${}和动态SQL语句，就是dynamic的
		if (isDynamic) {
			sqlSource = new DynamicSqlSource(configuration, rootSqlNode);
		} else {
			// 否则是RawSqlSource的（带有#{}的SQL语句）
			sqlSource = new RawSqlSource(configuration, rootSqlNode, parameterType);
		}
		return sqlSource;
	}

	/**
	 * 解析动态标签sqlNode Eg:if,forEach,where,set等标签
	 * @param node select/update/delete/insert 节点
	 * @return
	 */
	protected MixedSqlNode parseDynamicTags(XNode node) {
		List<SqlNode> contents = new ArrayList<>();
		//获取<select>\<insert>等4个标签的子节点，子节点包括元素节点和文本节点
		NodeList children = node.getNode().getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			// 创建 XNode，该过程会将能解析掉的“${}”都解析掉
			XNode child = node.newXNode(children.item(i));
			// 处理文本节点
			if (child.getNode().getNodeType() == Node.CDATA_SECTION_NODE
					|| child.getNode().getNodeType() == Node.TEXT_NODE) {
				String data = child.getStringBody("");
				// 将文本内容封装到SqlNode中
				TextSqlNode textSqlNode = new TextSqlNode(data);
				// 解析 SQL 语句，如果含有未解析的“${}”站为符，则为动态SQL
				if (textSqlNode.isDynamic()) {
					contents.add(textSqlNode);
					// 标记为动态 SQL 语句
					isDynamic = true;
				} else {
					// 除了${}都是static的，包括下面的动态SQL标签
					contents.add(new StaticTextSqlNode(data));
				}

			} else
				//处理元素节点
				if (child.getNode().getNodeType() == Node.ELEMENT_NODE) {
				// 如果子节点是一个标签，那么一定是动态 SQL,并且根据不同的动态标签生成不同的 NodeHandler
				String nodeName = child.getNode().getNodeName();
				// 动态SQL标签处理器
				NodeHandler handler = nodeHandlerMap.get(nodeName);
				if (handler == null) {
					throw new BuilderException("Unknown element <" + nodeName + "> in SQL statement.");
				}
				// 处理动态 SQL,并将解析得到的 SqlNode 对象放入 contents 集合中保存
				handler.handleNode(child, contents);
				// 动态SQL标签是dynamic的
				isDynamic = true;
			}
		}
		return new MixedSqlNode(contents);
	}

	private interface NodeHandler {
		/**
		 * 处理动态 SQL,并将解析得到的 SqlNode 对象放入 contents 集合中保存
		 * @param nodeToHandle 节点信息
		 * @param targetContents 已经加载SqlNode的 contents 集合
		 */
		void handleNode(XNode nodeToHandle, List<SqlNode> targetContents);
	}

	private class BindHandler implements NodeHandler {
		public BindHandler() {
			// Prevent Synthetic Access
		}

		@Override
		public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
			final String name = nodeToHandle.getStringAttribute("name");
			final String expression = nodeToHandle.getStringAttribute("value");
			final VarDeclSqlNode node = new VarDeclSqlNode(name, expression);
			targetContents.add(node);
		}
	}

	private class TrimHandler implements NodeHandler {
		public TrimHandler() {
			// Prevent Synthetic Access
		}

		@Override
		public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
			MixedSqlNode mixedSqlNode = parseDynamicTags(nodeToHandle);
			String prefix = nodeToHandle.getStringAttribute("prefix");
			String prefixOverrides = nodeToHandle.getStringAttribute("prefixOverrides");
			String suffix = nodeToHandle.getStringAttribute("suffix");
			String suffixOverrides = nodeToHandle.getStringAttribute("suffixOverrides");
			TrimSqlNode trim = new TrimSqlNode(configuration, mixedSqlNode, prefix, prefixOverrides, suffix,
					suffixOverrides);
			targetContents.add(trim);
		}
	}

	private class WhereHandler implements NodeHandler {
		public WhereHandler() {
			// Prevent Synthetic Access
		}

		@Override
		public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
			// 调用 parseDynamicTags() 方法，解析<where>节点的子节点
			MixedSqlNode mixedSqlNode = parseDynamicTags(nodeToHandle);
			// 创建 WhereSqlNode,并添加到 targetContents 集合中保存
			WhereSqlNode where = new WhereSqlNode(configuration, mixedSqlNode);
			targetContents.add(where);
		}
	}

	private class SetHandler implements NodeHandler {
		public SetHandler() {
			// Prevent Synthetic Access
		}

		@Override
		public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
			MixedSqlNode mixedSqlNode = parseDynamicTags(nodeToHandle);
			SetSqlNode set = new SetSqlNode(configuration, mixedSqlNode);
			targetContents.add(set);
		}
	}

	private class ForEachHandler implements NodeHandler {
		public ForEachHandler() {
			// Prevent Synthetic Access
		}

		@Override
		public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
			MixedSqlNode mixedSqlNode = parseDynamicTags(nodeToHandle);
			String collection = nodeToHandle.getStringAttribute("collection");
			String item = nodeToHandle.getStringAttribute("item");
			String index = nodeToHandle.getStringAttribute("index");
			String open = nodeToHandle.getStringAttribute("open");
			String close = nodeToHandle.getStringAttribute("close");
			String separator = nodeToHandle.getStringAttribute("separator");
			ForEachSqlNode forEachSqlNode = new ForEachSqlNode(configuration, mixedSqlNode, collection, index, item,
					open, close, separator);
			targetContents.add(forEachSqlNode);
		}
	}

	private class IfHandler implements NodeHandler {
		public IfHandler() {
			// Prevent Synthetic Access
		}

		@Override
		public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
			MixedSqlNode mixedSqlNode = parseDynamicTags(nodeToHandle);
			String test = nodeToHandle.getStringAttribute("test");
			IfSqlNode ifSqlNode = new IfSqlNode(mixedSqlNode, test);
			targetContents.add(ifSqlNode);
		}
	}

	private class OtherwiseHandler implements NodeHandler {
		public OtherwiseHandler() {
			// Prevent Synthetic Access
		}

		@Override
		public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
			MixedSqlNode mixedSqlNode = parseDynamicTags(nodeToHandle);
			targetContents.add(mixedSqlNode);
		}
	}

	private class ChooseHandler implements NodeHandler {
		public ChooseHandler() {
			// Prevent Synthetic Access
		}

		@Override
		public void handleNode(XNode nodeToHandle, List<SqlNode> targetContents) {
			List<SqlNode> whenSqlNodes = new ArrayList<>();
			List<SqlNode> otherwiseSqlNodes = new ArrayList<>();
			handleWhenOtherwiseNodes(nodeToHandle, whenSqlNodes, otherwiseSqlNodes);
			SqlNode defaultSqlNode = getDefaultSqlNode(otherwiseSqlNodes);
			ChooseSqlNode chooseSqlNode = new ChooseSqlNode(whenSqlNodes, defaultSqlNode);
			targetContents.add(chooseSqlNode);
		}

		private void handleWhenOtherwiseNodes(XNode chooseSqlNode, List<SqlNode> ifSqlNodes,
				List<SqlNode> defaultSqlNodes) {
			List<XNode> children = chooseSqlNode.getChildren();
			for (XNode child : children) {
				String nodeName = child.getNode().getNodeName();
				NodeHandler handler = nodeHandlerMap.get(nodeName);
				if (handler instanceof IfHandler) {
					handler.handleNode(child, ifSqlNodes);
				} else if (handler instanceof OtherwiseHandler) {
					handler.handleNode(child, defaultSqlNodes);
				}
			}
		}

		private SqlNode getDefaultSqlNode(List<SqlNode> defaultSqlNodes) {
			SqlNode defaultSqlNode = null;
			if (defaultSqlNodes.size() == 1) {
				defaultSqlNode = defaultSqlNodes.get(0);
			} else if (defaultSqlNodes.size() > 1) {
				throw new BuilderException("Too many default (otherwise) elements in choose statement.");
			}
			return defaultSqlNode;
		}
	}

}
