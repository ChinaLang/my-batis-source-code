/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.scripting.xmltags;

/**
 * 记录非动态 Sql 语句节点
 * <pre>
 *     1. 没有"动态SQL"节点 和 “${}”占位符。则为"非动态SQL"(又称“静态SQL”).
 *     2. 如果节点只包含“#{}”占位符，而不包含“动态SQL”节点或为解析的“${}"占位符，则为”非动态SQL“
 *     ”动态SQL“包含 trim,where,sql,set等标签的语句都是"动态SQL"
 * </pre>
 * @author Clinton Begin
 */
public class StaticTextSqlNode implements SqlNode {
  private final String text;

  public StaticTextSqlNode(String text) {
    this.text = text;
  }

  @Override
  public boolean apply(DynamicContext context) {
    // 直接将 text 记录到 DynamicContext.sqlBuilder 字段中
    context.appendSql(text);
    return true;
  }

}