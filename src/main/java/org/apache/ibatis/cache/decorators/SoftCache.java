/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.cache.decorators;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.locks.ReadWriteLock;

import org.apache.ibatis.cache.Cache;

/**
 * Soft Reference cache decorator
 * Thanks to Dr. Heinz Kabutz for his guidance here.
 *
 * @author Clinton Begin
 */
public class SoftCache implements Cache {
  /**
   * <pre>
   *     在 SoftCache 中，最近使用的一部分缓存项不会被 GC 回收，
   *     这就是通过将其 value 添加到 hardLinksToAvoidGarbageCollection 集合中，
   *     实现的（即有强引用指向某value）hardLinksToAvoidGarbageCollection 集合是 LinkedList 类型
   * </pre>
   */
  private final Deque<Object> hardLinksToAvoidGarbageCollection;
  /**
   * 引用队列
   * <pre>
   *     用于记录已经被 GC 回收的缓存项所对应的SoftEntry对象
   * </pre>
   */
  private final ReferenceQueue<Object> queueOfGarbageCollectedEntries;
  /** 底层被装饰的底层 CaChe 对象 */
  private final Cache delegate;
  /** 强连接的个数，默认值256 */
  private int numberOfHardLinks;

  public SoftCache(Cache delegate) {
    this.delegate = delegate;
    this.numberOfHardLinks = 256;
    this.hardLinksToAvoidGarbageCollection = new LinkedList<>();
    this.queueOfGarbageCollectedEntries = new ReferenceQueue<>();
  }

  @Override
  public String getId() {
    return delegate.getId();
  }

  @Override
  public int getSize() {
    removeGarbageCollectedItems();
    return delegate.getSize();
  }


  public void setSize(int size) {
    this.numberOfHardLinks = size;
  }

  @Override
  public void putObject(Object key, Object value) {
    // 清除已经被 GC 回收的缓存项
    removeGarbageCollectedItems();
    // 向缓存中添加缓存项
    delegate.putObject(key, new SoftEntry(key, value, queueOfGarbageCollectedEntries));
  }

  @Override
  public Object getObject(Object key) {
    Object result = null;
    @SuppressWarnings("unchecked")
    // assumed delegate cache is totally managed by this cache
    // 从缓存中查找对应的缓存项
    SoftReference<Object> softReference = (SoftReference<Object>) delegate.getObject(key);
    // 检测缓存中是否有对应的缓存项
    if (softReference != null) {
      // 获取 softReference 引用的 value
      result = softReference.get();
      if (result == null) {
        // 已被 GC 回收
        // 从缓存中清除对应的缓存项
        delegate.removeObject(key);
      } else {
        // 未被 GC 回收
        // See #586 (and #335) modifications need more than a read lock 
        synchronized (hardLinksToAvoidGarbageCollection) {
          // 缓存项的 value 添加到 hardLinksToAvoidGarbageCollection 集合中保存
          hardLinksToAvoidGarbageCollection.addFirst(result);
          if (hardLinksToAvoidGarbageCollection.size() > numberOfHardLinks) {
            // 超过 numberOfHardLinks， 则将最老的缓存项从 hardLinksToAvoidGarbageCollection 集合中清除
            hardLinksToAvoidGarbageCollection.removeLast();
          }
        }
      }
    }
    return result;
  }

  @Override
  public Object removeObject(Object key) {
    // 清除已经被 GC 回收的缓存项
    removeGarbageCollectedItems();
    return delegate.removeObject(key);
  }

  @Override
  public void clear() {
    synchronized (hardLinksToAvoidGarbageCollection) {
      // 清理强引用集合
      hardLinksToAvoidGarbageCollection.clear();
    }
    // 清理被 GC 回收的缓存项
    removeGarbageCollectedItems();
    // 清理底层 delegate 缓存中的缓存项
    delegate.clear();
  }

  @Override
  public ReadWriteLock getReadWriteLock() {
    return null;
  }

  /**
   * 清除已经被 GC 回收的缓存项
   */
  private void removeGarbageCollectedItems() {
    SoftEntry sv;
    // 遍历 queueOfGarbageCollectedEntries 集合
    while ((sv = (SoftEntry) queueOfGarbageCollectedEntries.poll()) != null) {
      // 将已经被 GC 回收的 value 对象对应的缓存项清除
      delegate.removeObject(sv.key);
    }
  }

  /**
   * SoftCache 中缓存项的 value 是 SoftEntry 对象，SoftEntry 继承了 SoftReference.
   * 其中指向key 的引用是强引用，而指向 value 的引用是软引用。
   */
  private static class SoftEntry extends SoftReference<Object> {
    private final Object key;

    SoftEntry(Object key, Object value, ReferenceQueue<Object> garbageCollectionQueue) {
      // 指向 value 的引用是软引用，且关联了引用队列
      super(value, garbageCollectionQueue);
      // 强引用
      this.key = key;
    }
  }

}