/**
 *    Copyright 2009-2021 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.cache.decorators;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;

import org.apache.ibatis.cache.Cache;

/**
 * Lru (least recently used) cache decorator
 * 近期最少使用原则
 * @author Clinton Begin
 */
public class LruCache implements Cache {

  /** 底层被修饰的底层 Cache 对象 */
  private final Cache delegate;
  /**
   * 记录key最近的使用情况
   * <pre>
   * LinkedHashMap 类型对象。
   * 他是一个有序的HashMap，用于记录key最近的使用情况
   * </pre>
   */
  private Map<Object, Object> keyMap;
  /** 记录最少被使用的缓存项的key */
  private Object eldestKey;

  /**
   * 默认缓存大小是1024，可通过 setSize() 方法重新设置缓存大小
   */
  public LruCache(Cache delegate) {
    this.delegate = delegate;
    setSize(1024);
  }

  @Override
  public String getId() {
    return delegate.getId();
  }

  @Override
  public int getSize() {
    return delegate.getSize();
  }

  /**
   * 设置缓存大小
   * <pre>
   *     重新设置缓存大小，会重置keyMap字段
   * </pre>
   */
  public void setSize(final int size) {
    // 注意：LinkedHashMap 构造函数的第三个参数，true表示该LinkedHashMap记录的顺序是access-order
    // 也就是说，LinkedHashMap.get() 方法会改变其记录的顺序
    keyMap = new LinkedHashMap<Object, Object>(size, .75F, true) {
      private static final long serialVersionUID = 4267176411845948333L;

      /**
       * 移除最年长的对象
       * <pre>
       *     当调用LinkedHashMap.put() 方法时，会调用该方法
       * </pre>
       */
      @Override
      protected boolean removeEldestEntry(Map.Entry<Object, Object> eldest) {
        boolean tooBig = size() > size;
        // 如果已到达缓存上限，则更新 eldestKey 字段(LinkedHashMap 头队列元素)
        if (tooBig) {
          eldestKey = eldest.getKey();
        }
        return tooBig;
      }
    };
  }

  @Override
  public void putObject(Object key, Object value) {
    // 添加缓存
    delegate.putObject(key, value);
    // 删除最久未使用的缓存项
    cycleKeyList(key);
  }

  @Override
  public Object getObject(Object key) {
    // 修改 LinkedHashMap中记录的顺序(将key移动到队尾)
    keyMap.get(key);
    return delegate.getObject(key);
  }

  @Override
  public Object removeObject(Object key) {
    return delegate.removeObject(key);
  }

  @Override
  public void clear() {
    delegate.clear();
    keyMap.clear();
  }

  @Override
  public ReadWriteLock getReadWriteLock() {
    return null;
  }

  /**
   * 删除最久未使用的缓存项
   */
  private void cycleKeyList(Object key) {
    keyMap.put(key, key);
    // eldestKey不为null, 表示已达到缓存上限
    if (eldestKey != null) {
      // 删除最久未使用的缓存
      delegate.removeObject(eldestKey);
      eldestKey = null;
    }
  }

}
